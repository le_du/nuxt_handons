# 03-api-connection


## やること

axoisを導入して、WebAPIを繋ぎこみましょう。


## 学習のポイント

- [ ] `asyncData`メソッドをつかうと、コンポーネントのロード前に処理ができる
- [ ] `asyncData`メソッドのreturnで、`data`を更新することができる


## 手順

`02-routing`で使用したプロジェクトに追加実装していきます。

- [03-api-connection](#03-api-connection)
  - [やること](#やること)
  - [学習のポイント](#学習のポイント)
  - [手順](#手順)
  - [APIモックサーバの起動](#apiモックサーバの起動)
  - [axiosのインストール](#axiosのインストール)
  - [アイテム一覧の繋ぎこみ](#アイテム一覧の繋ぎこみ)
    - [ここまで実装できたら](#ここまで実装できたら)
  - [アイテム詳細の繋ぎこみ](#アイテム詳細の繋ぎこみ)
    - [ここまで実装できたら](#ここまで実装できたら-1)
  - [(再) 学習のポイント](#再-学習のポイント)


## APIモックサーバの起動

- `99-api-mock`に実装済みのAPIモックがあります。
- `$ npm install`後に`$ npm start`を実行することで、`http://localhost:3001`に起動します。
- 起動できたら`curl`コマンドを使って、Itemsのデータが取得できるか確認してください。

```sh
$ cd 99-api-mock
$ npm install
$ npm start

$ curl http://localhost:3001/items
$ curl http://localhost:3001/items/1
```


## axiosのインストール

```sh
$ npm install @nuxtjs/axios@5.12.2
```

**/package.json**

```json
{
    "name": "hello-nuxt",
    "dependencies": {
        "@nuxtjs/axios": "^5.12.2",
        "nuxt": "2.14.5"
    },
    "scripts": {
        "start": "nuxt"
    }
}

```
- `nuxt.config.js` ファイル作成
  
```js
module.exports = {
    modules: [
        '@nuxtjs/axios',
    ],

    axios: {
        // proxyHeaders: false
    }
}
```


## アイテム一覧の繋ぎこみ

- `/pages/items/index.vue`の`script`タグ内を以下のように実装してください。
- `asyncData`はコンポーネントのロード前に呼び出され、結果が`data`とマージされるメソッドです。

**/pages/items/index.vue**

```html
<template>
  <div>
    <h1>Items</h1>
    <table border="1">
      <thead>
        <tr>
          <th>id</th>
          <th>name</th>
          <th>price</th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="item in items" :key="item.id">
          <td>{{item.id}}</td>
          <td>
            <nuxt-link :to="'/items/' + item.id">{{item.name}}</nuxt-link>
          </td>
          <td>{{item.price}}</td>
        </tr>
      </tbody>
    </table>
    <button>
      <nuxt-link to="/items/new">Add Item</nuxt-link>
    </button>
  </div>
</template>

<script>
export default {
  data() {
    return {
      items: []
    };
  },
  async asyncData({ $axios }) {
    const response = await $axios.$get("http://localhost:3001/items");
    return { items: response };
  }
};
</script>
```

### ここまで実装できたら

- アイテム一覧にWebAPIから取得したデータが表示されていることを確認してください。(５つのItemが表示されます)


## アイテム詳細の繋ぎこみ

- `/pages/items/_id.vue`を以下のように実装してください。
- `asyncData`内では`context`オブジェクトを使用することができます。
- `context.params`は`$route.params`のエイリアスになっています。
- `template`は取得したデータを表示できるように追加実装しています。

**/pages/items/_id.vue**

```html
<template>
  <div>
    <h1>Items Id</h1>
    <ul>
      <li>id: {{item.id}}</li>
      <li>name: {{item.name}}</li>
      <li>price: {{item.price}}</li>
      <li>description: {{item.description}}</li>
    </ul>
  </div>
</template>

<script>
export default {
  data: function() {
    return {
      item: {}
    };
  },
  asyncData: async function(context) {
    const response = await context.$axios.$get(
      `http://localhost:3001/items/${context.params.id}`
    );
    return { item: response };
  }
};
</script>
```

### ここまで実装できたら

- 各アイテム詳細にWebAPIから取得したデータが表示されていることを確認してください。


## (再) 学習のポイント

- [x] `asyncData`メソッドをつかうと、コンポーネントのロード前に処理ができる
- [x] `asyncData`メソッドのreturnで、`data`を更新することができる
