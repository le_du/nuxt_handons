
module.exports = {
    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/toast'
    ],

    axios: {
        // proxyHeaders: false
    },
    toast: {
        position: 'top-center'
    },
    plugins: ['~/plugins/repositories'],

    buildModules: [
        // With options
        ['@nuxtjs/vuetify',]
    ],
    vuetify: {
        theme: {
            themes: {
                light: {
                    primary: '#084b83',
                    secondary: '#42bfdd',
                    accent: '#bbe6e4',
                    error: '#f4362a',
                    success: '#f0cc9b',
                    background: '#ffffff'
                }
            }
        }
    },
}