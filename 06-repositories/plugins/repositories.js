import createRepository from '~/repositories'
export default (ctx, inject) => {
    const repository = createRepository(ctx.$axios, ctx.$toast)
    inject('repositories', repository)
}
