import { Item as ItemApi } from './api'

export default ($axios, $toast = null) => {
    // api repositories
    const ItemApiRepository = new ItemApi('http://localhost:3001/items', $axios)

    const repositories = {
        getAllItem: ItemApiRepository.getAllItem,
        addNewItem: ItemApiRepository.addNewItem,
        updateItem: ItemApiRepository.updateItem,
        deleteItem: ItemApiRepository.deleteItem,
    }
    return (name) => (...params) => {
        const repo = repositories[name]
        return new Promise(async (resovle, reject) => {
            try {
                const res = await repo(...params)
                resovle(res)
            }
            catch (e) {
                reject(e)
                if ($toast != null) {
                    $toast.error(e)
                }
            }

        })
    }
}
