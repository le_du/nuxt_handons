import API from './api'
export default class Item extends API {
    getAllItem = () => {
        return this.axios.get(this.url)
    }
    addNewItem = (item) => {
        return this.axios.post(this.url, item)
    }
    updateItem = (item, id) => {
        const url = this.url + '/' + id
        return this.axios.put(url, item)
    }
    deleteItem = (id) => {
        const url = this.url + '/' + id
        return this.axios.delete(url)
    }
}