# 05 - CRUD

## やること

Vuetify を使って、ITEMS CRUD アクションテブールを実装しましょう。

## 手順

`04-vuetify`で使用したプロジェクトに追加実装していきます。

- [05 - CRUD](#05---crud)
  - [やること](#やること)
  - [手順](#手順)
  - [READ](#read)
  - [CREATE](#create)
  - [UPDATE](#update)
  - [DELETE](#delete)

## READ

```html

<template>
  <v-data-table :headers="headers" :items="items" class="elevation-1"></v-data-table>
</template>

<script>
export default {
  data: () => ({
    headers: [
      {
        text: "Id",
        align: "start",
        sortable: false,
        value: "id"
      },
      { text: "Name", value: "name", sortable: false },
      { text: "Price", value: "price", sortable: false },
      { text: "Description", value: "description", sortable: false },
      { text: "Actions", value: "actions", sortable: false }
    ],
    items: []
  }),

  created() {
    this.fetchItems();
  },

  methods: {
    async fetchItems() {
      const response = await this.$axios.$get("http://localhost:3001/items");
      this.items = response;
    }
  }
};
</script>
```

- `created` フックはインスタンスが生成された後にコードを実行したいときに使われます

## CREATE
-  `v-data-table` タグの中に　`v-slot:top` を追加します。
-  新しいitem追加用 `dialog` 作成します。

```html
    <template v-slot:top>
      <v-toolbar flat color="white">
        <v-toolbar-title>ITEMS CRUD</v-toolbar-title>
        <v-divider class="mx-4" inset vertical></v-divider>
        <v-spacer></v-spacer>
        <v-dialog v-model="dialog" max-width="500px">
          <template v-slot:activator="{ on, attrs }">
            <v-btn color="primary" dark class="mb-2" v-bind="attrs" v-on="on">New Item</v-btn>
          </template>
          <v-card>
            <v-card-title>
              <span class="headline">Add New Item</span>
            </v-card-title>

            <v-card-text>
              <v-container>
                <v-row>
                  <v-col cols="12" sm="6" md="4">
                    <v-text-field v-model="dialogItem.name" label="Name"></v-text-field>
                  </v-col>
                  <v-col cols="12" sm="6" md="4">
                    <v-text-field v-model="dialogItem.price" label="Price"></v-text-field>
                  </v-col>
                  <v-col cols="12" sm="6" md="4">
                    <v-text-field v-model="dialogItem.description" label="Description"></v-text-field>
                  </v-col>
                </v-row>
              </v-container>
            </v-card-text>

            <v-card-actions>
              <v-spacer></v-spacer>
              <v-btn color="blue darken-1" text @click="close">Cancel</v-btn>
              <v-btn color="blue darken-1" text @click="save">Add new</v-btn>
            </v-card-actions>
          </v-card>
        </v-dialog>
      </v-toolbar>
    </template>
```

- `script` 所も修正あります。
```html

<script>
export default {
  data: () => ({
    headers: [
      {
        text: "Id",
        align: "start",
        sortable: false,
        value: "id"
      },
      { text: "Name", value: "name", sortable: false },
      { text: "Price", value: "price", sortable: false },
      { text: "Description", value: "description", sortable: false },
      { text: "Actions", value: "actions", sortable: false }
    ],
    items: [],
    dialog: false,
    dialogItem: {
      name: "",
      price: 0,
      description: ""
    },
    dialogItemDefault: {
      name: "",
      price: 0,
      description: ""
    }
  }),

  created() {
    this.fetchItems();
  },

  methods: {
    // fetch items and set data to table
    async fetchItems() {
      const response = await this.$axios.$get("http://localhost:3001/items");
      this.items = response;
    },
    // reset dialog data whenever form is submitted or closed
    resetDialogForm() {
      Object.assign(this.dialogItem, this.dialogItemDefault);
    },
    // close dialog
    close() {
      this.dialog = false;
      this.resetDialogForm();
    },
    // actions when press save button at dialog
    save() {
      // add item
      this.addItem(this.dialogItem);
      // refetch items data to table
      this.fetchItems();
      // close dialog
      this.close();
    },

    // add new item with post
    addItem(item) {
      this.$axios.$post("http://localhost:3001/items", item);
    }
  }
};
</script>
```

## UPDATE
- READの同じ `dialog`　を追加するか？
  - メリット：
    - 処理が明確。
  - デメリット：
    - 重複関数が多い（close, save, resetForm。
    - 将来的には，例えばitem の要素が変わる場合、修正必要箇所も二倍になります。


    →　前の `dialog`を使用したほうがいいわけです。

- `dialog` の変更
    ```html
    <template v-slot:top>
      <v-toolbar flat color="white">
        <v-toolbar-title>ITEMS CRUD</v-toolbar-title>
        <v-divider class="mx-4" inset vertical></v-divider>
        <v-spacer></v-spacer>
        <v-dialog v-model="dialog" max-width="500px">
          <template v-slot:activator="{ on, attrs }">
            <v-btn color="primary" dark class="mb-2" v-bind="attrs" v-on="on">New Item</v-btn>
          </template>
          <v-card>
            <v-card-title>
              <span class="headline">{{ formTitle }}</span>
            </v-card-title>

            <v-card-text>
              <v-container>
                <v-row>
                  <v-col cols="12" sm="6" md="4">
                    <v-text-field v-model="dialogItem.name" label="Name"></v-text-field>
                  </v-col>
                  <v-col cols="12" sm="6" md="4">
                    <v-text-field v-model="dialogItem.price" label="Price"></v-text-field>
                  </v-col>
                  <v-col cols="12" sm="6" md="4">
                    <v-text-field v-model="dialogItem.description" label="Description"></v-text-field>
                  </v-col>
                </v-row>
              </v-container>
            </v-card-text>

            <v-card-actions>
              <v-spacer></v-spacer>
              <v-btn color="blue darken-1" text @click="close">Cancel</v-btn>
              <v-btn color="blue darken-1" text @click="save">{{formSaveTitle}}</v-btn>
            </v-card-actions>
          </v-card>
        </v-dialog>
      </v-toolbar>
    </template>
    ```
- `v-data-table` タグの中に　`v-slot:item.actions` を追加します。
    ```html
    <template v-slot:item.actions="{ item }">
      <v-icon small class="mr-2" @click="editItem(item)">mdi-pencil</v-icon>
    </template>
    ```

- `script` に ロジック追加。
    ```html
    <script>
        export default {
        data: () => ({
            headers: [
            {
                text: "Id",
                align: "start",
                sortable: false,
                value: "id"
            },
            { text: "Name", value: "name", sortable: false },
            { text: "Price", value: "price", sortable: false },
            { text: "Description", value: "description", sortable: false },
            { text: "Actions", value: "actions", sortable: false }
            ],
            items: [],
            dialog: false,
            editItemIndex: null,
            dialogItem: {
            name: "",
            price: 0,
            description: ""
            },
            dialogItemDefault: {
            name: "",
            price: 0,
            description: ""
            }
        }),

        created() {
            this.fetchItems();
        },
        computed: {
            formTitle() {
            return this.editItemIndex != null ? "EDIT ITEM" : "ADD NEW ITEM";
            },
            formSaveTitle() {
            return this.editItemIndex != null ? "Update" : "Add new";
            }
        },

        methods: {
            // fetch items and set data to table
            async fetchItems() {
            const response = await this.$axios.$get("http://localhost:3001/items");
            this.items = response;
            },
            // reset dialog data whenever form is submitted or closed
            resetDialogForm() {
            this.dialogItem = Object.assign({}, this.dialogItemDefault);
            },
            // reset editItemIndex
            resetEditItemIndex() {
            this.editItemIndex = null;
            },
            // actions when press item edit button
            editItem(item) {
            this.editItemIndex = item.id;
            this.dialogItem = Object.assign({}, item);
            this.dialog = true;
            },
            // close dialog
            close() {
            this.dialog = false;
            this.resetDialogForm();
            this.resetEditItemIndex();
            },
            // actions when press save button at dialog
            save() {
            if (this.editItemIndex != null) {
                this.updateItem(this.dialogItem, this.editItemIndex);
            } else {
                // add item
                this.addItem(this.dialogItem);
            }
            // refetch items data to table
            this.fetchItems();
            // close dialog
            this.close();
            },

            // add new item with post
            addItem(item) {
            this.$axios.$post("http://localhost:3001/items", item);
            },
            // update new item with put
            updateItem(item, index) {
            this.$axios.$put(`http://localhost:3001/items/${index}`, item);
            }
        }
        };
    </script>
    ```

## DELETE

- `v-slot:item.actions`　に `delete` コンポーネントを追加します。
    ```html
     <template v-slot:item.actions="{ item }">
      <v-icon small class="mr-2" @click="editItem(item)">mdi-pencil</v-icon>
      <v-icon small @click="deleteItem(item)">mdi-delete</v-icon>
    </template>
    ```

- `deleteItem` メソッド定義
    ```js
    deleteItem(item) {
        confirm("Are you sure you want to delete this item?") &&
            this.$axios.$delete(`http://localhost:3001/items/${item.id}`);
        this.fetchItems();****
        }
    ```