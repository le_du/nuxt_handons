# 04-vuetify


## やること

Vuetify.jsを使って、マテリアルデザインに適応したコンポーネントを実装しましょう。


## 手順

`03-api-connection`で使用したプロジェクトに追加実装していきます。

- [04-vuetify](#04-vuetify)
  - [やること](#やること)
  - [手順](#手順)
  - [Vuetify.jsの導入](#vuetifyjsの導入)
  - [Vuetify.jsの使い方](#vuetifyjsの使い方)
  - [基本レイアウトの実装](#基本レイアウトの実装)
  - [NavigationDrawerとToolbarの導入](#navigationdrawerとtoolbarの導入)
  - [チャレンジ](#チャレンジ)


## Vuetify.jsの導入

- プロジェクトにVuetify.jsを導入します。
- 今回は[`@nuxtjs/vuetify`パッケージ](https://vuetifyjs.com/ja/getting-started/quick-start/#nuxt-%E3%81%AE%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB)を使用します。
- 追加したら、`$ npm install @nuxtjs/vuetify@1.11.2`を実行してください。

**/package.json**

```json
{
    "name": "hello-nuxt",
    "dependencies": {
        "@nuxtjs/axios": "^5.12.2",
        "@nuxtjs/vuetify": "^1.11.2",
        "nuxt": "2.14.5"
    },
    "scripts": {
        "start": "nuxt"
    }
}

```

- `/nuxt.config.js`に以下の内容を書いて保存してください。
- `vuetify.theme`にカラーテーマを設定することができます。
- 便利な[Theme generator](https://theme-generator.vuetifyjs.com/)が利用できます。

**/nuxt.config.js**

```js

module.exports = {
    modules: [
        '@nuxtjs/axios',
    ],

    axios: {
        // proxyHeaders: false
    },
    buildModules: [
        // With options
        ['@nuxtjs/vuetify',]
    ],
    vuetify: {
        theme: {
            themes: {
                light: {
                    primary: '#084b83',
                    secondary: '#42bfdd',
                    accent: '#bbe6e4',
                    error: '#f4362a',
                    success: '#f0cc9b',
                    background: '#ffffff'
                }
            }
        }
    },
}
```

これで`Vuetify.js`を使う準備が整いました。


## Vuetify.jsの使い方

- [Vuetify.jsのサイト](https://vuetifyjs.com/ja/)の`UI components`のメニューからコンポーネントを探して実装していきます。
- コンポーネントの詳細ページでデモやサンプルコード、APIを確認することができます。
- 基本的にはサンプルコードをベースに、APIを見ながらカスタマイズした上でプロジェクトに実装していきます。


## 基本レイアウトの実装

- [Pre-defined layouts](https://vuetifyjs.com/ja/layout/pre-defined)の`Default application markup`を参考にレイアウトを実装していきます。
- 全体を`v-app`タグでラップし、`v-navigation-drawer`と`v-toolbar`については`menu-list`で実装することとします。

**/layouts/default.vue**

```html
<template>
  <v-app>
    <menu-list />
    <v-content>
      <v-container fluid>
        <nuxt />
      </v-container>
    </v-content>
  </v-app>
</template>

<script>
import MenuList from "~/components/MenuList";

export default {
  components: { MenuList }
};
</script>
```


## NavigationDrawerとToolbarの導入

- [Toolbar](https://vuetifyjs.com/ja/components/toolbars)と[Navigation drawer](https://vuetifyjs.com/ja/components/navigation-drawers)を導入します。
- `/components/MenuList.vue`を以下のように実装してください。
- `data`でdrawerの表示と非表示のフラグを定義し、`v-navigation-drawer`タグの`v-model`に渡しています。
- `v-toolbar-side-icon`タグの`@click`でフラグを切り替えています。

**/components/MenuList.vue**

```html
<template>
  <div>
    <v-navigation-drawer app v-model="drawer" absolute bottom temporary>
      <v-list nav dense>
        <v-list-item-group>
          <v-list-item @click="route('/')">
            <v-list-item-title>HOME</v-list-item-title>
          </v-list-item>
          <v-list-item @click="route('/items')">
            <v-list-item-title>ITEMS</v-list-item-title>
          </v-list-item>
        </v-list-item-group>
      </v-list>
    </v-navigation-drawer>
    <v-toolbar app dark color="primary">
      <v-app-bar-nav-icon @click.stop="drawer = !drawer"></v-app-bar-nav-icon>
      <v-toolbar-title>Item Management System</v-toolbar-title>
    </v-toolbar>
  </div>
</template>

<script>
export default {
  data: function() {
    return {
      drawer: false
    };
  },
  methods: {
    route(url) {
      this.$router.push(url);
    }
  }
};
</script>
```


## チャレンジ

- Vuetify.jsのサイトを参照しながら、ページを装飾していきましょう。
- サンプルコードでは以下のコンポーネントを使用しています。
  - アイテム一覧：[Data table](https://vuetifyjs.com/ja/components/data-tables)
  - アイテム新規作成：[Form](https://vuetifyjs.com/ja/components/forms)
  - ボタン：[Button](https://vuetifyjs.com/ja/components/buttons)
